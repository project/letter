LETTER
----------------------

This is a very simple paper-on-wood theme for D7.  It's based on photo of a 
piece of stationary on my own desk.  The "paper" can be of any length and has a 
"torn" edge at the bottom.  It's a tableless, fixed width theme with blocks 
above and below the content, both on and off the "page." Features include:
- Suckerfish drop-down menu support
- Webfont support through the theme settings form (see below)

Letter is a Zen 3.x sub-theme; you must install Zen 3.x for Letter to work.

Webfont support
----------------------

Webfonts is a relatively new technology that lets you use any font on a web
page.  Letter includes Garamond, which is a webfont hosted by Google Webfonts.
You can easily override with your own webfont provider.  Here's how:

1. Go to the theme settings page: admin/appearance/settings/letter
2. Under "Webfont Settings," paste in the link to your webfont provider.  Letter
	supports either css (like Google Webfonts) or javascript (like Fonts.com).
3. If you use the css method, you also need to edit the theme css files.  Font
	family is set in css/html-reset.css.  
	(If you use javascript, you don't need to edit the css files.)

For more information about webfonts: http://koplowicz.com/node/706
For more information about Garamond: http://koplowicz.com/node/708
